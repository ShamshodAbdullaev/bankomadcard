package uz.pdp.model;

import java.util.Scanner;

public class Card {
    Scanner scanner = new Scanner(System.in);

    private String owner; //Shamshod Abdullayev
    private String number; //8600 0210 6768 1298
    private String expireDate; //11/25
    private double balance;
    private String password = "1111";
    private boolean isBlocked = true;


    public Card() {
    }

    public Card(String owner, String number, String expireDate, double balance) {
        this.owner = owner;
        this.number = number;
        this.expireDate = expireDate;
        this.balance = balance;
        this.password = "1111";
    }

//    THIS FUNCTION

    public void changePassword() {
        int count = 1;
        while (count < 4) {
        scanner = new Scanner(System.in);
        System.out.print("Parolni kiriting: ");
        if (this.password.equals(scanner.nextLine())) {
            boolean isEqual = false;
            while (!isEqual) {
                System.out.print("New Password: ");
                String newPassword = scanner.nextLine();
                System.out.print("PrePassword: ");
                String prePassword = scanner.nextLine();
                if (newPassword.equals(prePassword)) {
                    this.password = newPassword;
                    isEqual = true;
                    System.out.println("Parol o'zgardi!!!");
                    return;
                } else {
                    System.out.println("parolni kiritishda xatolik!!!");
                }
            }
            break;
        } else {
            System.out.println("Noto'g'ri parol kiritdingiz. ");
        }
            count += 1;
        }
        System.out.println("Card BLOCK ");
    }

    public void addAmount() {
        int count = 1;
        while (count < 4) {
            scanner = new Scanner(System.in);
            System.out.print("Parolni kiriting: ");
            String password = scanner.nextLine();
            if (this.password.equals(password)) {
                System.out.print("Qancha pul qo'ymoqchisiz: ");
                double amount = scanner.nextDouble();
                if (amount > 0) {
                    this.balance += amount;
                    System.out.println("Hisobingiz: " + amount + " so'mga to'ldirildi.");
                    System.out.println("Sizning hisobingizda: " + balance + " so'm bor.");
                    return;
                } else {
                    System.out.println("Noto'g'ri summa kiritdingiz!!!");
                }
                return;
            } else {
                System.out.println("Noto'g'ri parol kiritdingiz. ");
            }
            count += 1;
        }
        System.out.println("Card BLOCK ");
    }

    public void withdrawAmount() {
        int count = 1;
        while (count < 4) {
            scanner = new Scanner(System.in);
            System.out.print("Parolni kiriting: ");
            String password = scanner.nextLine();
            if (this.password.equals(password)) {
                System.out.print("Summani kiriting: ");
                double amount = scanner.nextDouble();
                if (amount > 0 && this.balance > (amount + amount * 0.01)) {
                    this.balance -= (amount + amount * 0.01);
                    System.out.println("Hisobingizdan: " + (amount + amount * 0.01) + " so'm yechildi.");
                    System.out.println("Sizning hisobda " + balance + " so'm qoldi.");
                    return;
                } else {
                    System.out.println("Hisobingizda mablag' yetarli emas!!!");
                }
                break;
            } else {
                System.out.println("Noto'g'ri parol kiritdingiz. ");
            }
            count += 1;
        }
        System.out.println("Card BLOCK ");
    }

    public void checkBalance() {
        int count = 1;
        while (count < 4) {
        scanner = new Scanner(System.in);
        System.out.print("Parolni kiriting: ");
        String password = scanner.nextLine();
        if (this.password.equals(password)) {
            System.out.println("Sizning hisobda " + balance + " so'm qoldi.");
            return;
        } else {
            System.out.println("Noto'g'ri parol kiritdingiz. ");
        }
            count += 1;
        }
        System.out.println("Card BLOCK ");
    }


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Card{" +
                "owner='" + owner + '\'' +
                ", number='" + number + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", balance=" + balance +
                ", password='" + password + '\'' +
                ", isBlocked=" + isBlocked +
                ", scanner=" + scanner +
                '}';
    }
}
