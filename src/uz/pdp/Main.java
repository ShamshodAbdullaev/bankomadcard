package uz.pdp;

import uz.pdp.model.Card;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Card card = new Card();
        System.out.print("FIO: ");
        card.setOwner(scanner.nextLine());
        System.out.print("Card Number: ");
        card.setNumber(scanner.nextLine());
        System.out.print("ExpireDate: ");
        card.setExpireDate(scanner.nextLine());


        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("0=> Kartani chiqarish, 1=> Change Password, " +
                    "2=> AddAmount(Xisobni to'ldirish), 3=> WithdrawAmount(Pul yechish)" +
                    "4=> CheckBalance(Hisobni tekshirish), 5=> Trade(Savdo), 6=> Info ");
            System.out.print("Menuni chaqirish: ");
            stepCode = scanner.nextInt();

            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    card.changePassword();
                    break;
                case 2:
                    card.addAmount();
                    break;
                case 3:
                    card.withdrawAmount();
                    break;
                case 4:
                    card.checkBalance();
                    break;
                case 5:
                    System.out.println("Savdoni yozish!!! dankasalik qilmasdan yozib quygin");
                    break;
                case 6:
                    System.out.println(card.toString());
                    break;
                default:
                    System.out.println("Noto'g'ri son kiritdingiz. Menudagi sonlarni kiriting.");
            }
        }
    }
}
